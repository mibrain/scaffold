from scaffold.utils import classname_to_uid

class ScaffoldDefiningClass(type):
    "Metaclass for Scaffold classes"
    def __new__(cls, name, bases, attrs):
        if '__abstract__' not in attrs:
            attrs['__abstract__'] = False
        new_class = super(ScaffoldDefiningClass, cls).__new__(cls, name, bases, attrs)
        if not new_class.__abstract__:
            if new_class.uid is None:
                new_class.uid = classname_to_uid(new_class.__name__)
        return new_class

class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

def enum(**kwargs):
    enums = {}
    attrs = {}
    for key, (value, name) in kwargs.items():
        enums[value] = name
        attrs[key] = value

    @classmethod
    def tuples(cls):
        return cls.ENUMS.items()

    attrs['ENUMS'] = enums
    attrs['tuples'] = tuples
    return type('Enum', (object, ), attrs)


def overrides(interface_class):
    def overrider(method):
        # FIXME: need to test whether the class which the method belongs to is the subclass of interface_class
        assert method.__name__ in dir(interface_class), \
            "The method '%s' cannot be overrided because it doesn't exist in the parent class '%s'" \
            % (method.__name__, interface_class.__name__)
        return method
    return overrider
