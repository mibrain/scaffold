# -*- coding: utf-8 -*-
import re

def list_first_item(value):
    try:
        if not hasattr(value[0], '__iter__'):
            return [value[0]]
        else:
            return value[0]
    except Exception:
        return None

def float_equals(a, b):
    return abs(a - b) <= 1e-6

def classname_to_uid(class_name):
    return class_name.lower()

def force_utf8(text):
    if isinstance(text, unicode):
        text = text.encode("utf-8")
    return text
