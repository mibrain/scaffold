# -*- coding: utf-8 -*-

import sys
import uuid
from abc import ABCMeta, abstractmethod
from django.forms import Media
from django.template import Context, loader
from django.http import HttpResponse
from django.utils import six
from django.conf.urls import patterns, url
from django.utils.safestring import mark_safe
from django.utils.encoding import python_2_unicode_compatible
from django.views.decorators.cache import never_cache
from scaffold.types import overrides, Singleton, ScaffoldDefiningClass
from scaffold.utils import force_utf8, classname_to_uid
from django.conf import settings

GRID_COLUMN_NUM = 24

class UIException(Exception):
    "UI Exception"

class UIDefiningClass(ScaffoldDefiningClass, ABCMeta):
    "Metaclass for UI classes"
    def __new__(cls, name, bases, attrs):
        new_class = super(UIDefiningClass, cls).__new__(cls, name, bases, attrs)
        if not new_class.__abstract__:
            if new_class.uid is None:
                new_class.uid = classname_to_uid(new_class.__name__)
        if isinstance(new_class.label, six.string_types):
            new_class.label = force_utf8(new_class.label)
        else:
            new_class.label = new_class.uid
        return new_class

@python_2_unicode_compatible
class BaseUI(object):
    "Base class for all UI classes"
    __metaclass__ = UIDefiningClass
    __abstract__ = True

    uid = None
    label = None
    desc = ''

    def __new__(cls, *args, **kwargs):
        if cls.__abstract__:
            raise UIException("Abstract UI class '%s' cannot be instantiated."
                              % cls.__name__)
        return super(BaseUI, cls).__new__(cls, *args, **kwargs)

    def __init__(self):
        self.media = self.aggregate_media()

    def __str__(self):
        return u"<%s '%s'>" % (self.__class__.__name__, self.uid)

    @abstractmethod
    def render(self, **context):
        pass

    def warn(self, msg):
        print >> sys.stderr, "[WARNING] <%s> %s" % (self.__class__.__name__, msg)

    def error(self, msg):
        raise UIException("<%s> %s" % (self.__class__.__name__, msg))

    @classmethod
    def aggregate_media(cls):
        # Get the media property of the superclass, if it exists
        base = Media()
        for sup_cls in cls.__bases__:
            try:
                base += sup_cls.aggregate_media()
            except AttributeError:
                pass

        # Get the media definition for this class
        definition = getattr(cls, 'Media', None)
        if definition:
            extend = getattr(definition, 'extend', True)
            if extend:
                if extend == True:
                    m = base
                else:
                    m = Media()
                    for medium in extend:
                        m = m + base[medium]
                return m + Media(definition)
            else:
                return Media(definition)
        else:
            return base


class UIView(BaseUI):
    "Base class for all the UI classes which can act as view"
    __abstract__ = True

    def as_view(self):
        """
        Main entry point for a request-response process.
        """
        @never_cache
        def view(request, *args, **kwargs):
            return HttpResponse(self.render(request=request))
        return view

    @abstractmethod
    def path(self):
        pass

    @property
    def urlpattern(self):
        return url(r'^%s%s$' % (settings.URL_PREFIX, self.path()), self.as_view(), name=self.path())

    @property
    def url(self):
        return '/' + settings.URL_PREFIX + self.path()

class PageGroup(object):
    def __init__(self, label, page_or_tuple):
        self.label = force_utf8(label)
        if isinstance(page_or_tuple, Page):
            self.pages = (page_or_tuple, )
        else:
            self.pages = page_or_tuple
        self.uid = uuid.uuid4().hex[:8]

    def page_uids(self):
        return [page.uid for page in self]

    def __iter__(self):
        return iter(self.pages)

class Portal(UIView):
    "Base class for portals"
    __abstract__ = True

    list_index_pages = []
    start_page = None
    list_pages = []

    def __init__(self):
        super(Portal, self).__init__()
        self._page_groups = []
        self._registry = {}
        self._pages = []
        # Register index pages
        for index, node in enumerate(self.list_index_pages):
            if isinstance(node, type) and issubclass(node, Page):
                page_class = node
                page = self.register(page_class)
                if page is not None:
                    self._page_groups.append(PageGroup(None, page))
            elif isinstance(node, (list, tuple)):
                if len(node) != 2:
                    self.error("'index_pages[%d]' does not have exactly two elements" % index)
                label, page_or_tuple = node
                if isinstance(page_or_tuple, type) and issubclass(page_or_tuple, Page):
                    page_classes = (page_or_tuple, )
                elif isinstance(page_or_tuple, (list, tuple)):
                    page_classes = page_or_tuple
                else:
                    continue
                pages = []
                for page_class in page_classes:
                    page = self.register(page_class)
                    if page is not None:
                        pages.append(page)
                if len(pages) > 0:
                    if not isinstance(label, six.string_types):
                        label = ''
                    self._page_groups.append(PageGroup(label, pages))
            else:
                self.error("'index_pages[%d]' must be a Page or tuple" % index)

        start_page_class = self.__class__.start_page
        if start_page_class is not None and issubclass(start_page_class, Page):
            self.start_page = self.register(start_page_class)
        else:
            self.start_page = self._pages[0] if len(self._pages) > 0 else None
        if len(self._page_groups) == 0 and self.start_page is None:
            self.error('no valid index pages')

        for node in self.list_pages:
            if isinstance(node, type) and issubclass(node, Page):
                self.register(node)

    def register(self, page_class):
        if not issubclass(page_class, Page):
            self.warn("the class to register is not a subclass of Page")
            return None
        if page_class.uid in self._registry:
            self.warn("ignore the page class whose uid is '%s' and already registered to %s"
                      % (page_class.uid, self))
            return self._registry[page_class.uid]
        page = page_class()
        page.portal = self
        self._registry[page.uid] = page
        self._pages.append(page)
        return page

    @overrides(UIView)
    def path(self):
        return self.uid

    @overrides(UIView)
    def render(self, **context):
        if self.start_page:
            return self.start_page.render(**context)
        else:
            return self.render_default()

    def render_default(self):
        return ''

    @property
    def page_groups(self):
        return self._page_groups

    def __iter__(self):
        return iter(self._pages)

class Page(UIView):
    "Base class for pages"
    __abstract__ = True
    sidebar = True
    navbar = True

    icon = ''
    list_modules = []
    template = loader.get_template("scaffold/page.html")

    def __init__(self):
        super(Page, self).__init__()
        self._modules = []
        self._rows = []
        self._registry = {}
        # Register modules
        for index, node in enumerate(self.list_modules):
            if isinstance(node, type) and issubclass(node, UIModule):
                module_class = node
                module = self.register(module_class)
                if module is not None:
                    self._rows.append((GRID_COLUMN_NUM, (module,)))
            elif isinstance(node, (list, tuple)):
                modules = []
                for module_class in node:
                    if issubclass(module_class, UIModule):
                        module = self.register(module_class)
                        if module is not None:
                            modules.append(module)
                l = len(modules)
                if l == 0:  # ignore empty tuple
                    continue
                if l >= 3:
                    self.error("'modules[%d]' does not have one or two elements" % index)
                self._rows.append((GRID_COLUMN_NUM / l, modules))
            else:
                self.error("'modules[%d]' must be a UIModule or tuple" % index)


    def register(self, module_class):
        if not issubclass(module_class, UIModule):
            self.warn("the class to register is not a subclass of UIModule")
            return None
        if module_class.uid in self._registry:
            self.warn("ignore the module class whose uid is '%s' and already registered to %s"
                      % (module_class.uid, self))
            return None
        module = module_class()
        self._registry[module.uid] = module
        self._modules.append(module)
        self.media += module.media
        return module

    @overrides(UIView)
    def path(self):
        return "%s/%s" % (self.portal.path(), self.uid)

    @overrides(UIView)
    def render(self, **context):
        context.update({
            'current_portal': self.portal,
            'current_page': self,
            'site': self.portal.site,
            'sidebar': self.sidebar,
            'navbar': self.navbar,
            'settings': settings,
        })
        if 'request' in context:
            context['user'] = context['request'].user
        else:
            context['user'] = None
        return self.template.render(Context(context))

    @property
    def media_js(self):
        media = Media()
        try:
            media += self.media['js']
        except:
            pass
        return media

    @property
    def media_css(self):
        media = Media()
        try:
            media += self.media['css']
        except:
            pass
        return media

    @property
    def modules(self):
        return self._modules

    def __iter__(self):
        return iter(self._rows)

    class Media:
        js = [
            'jquery/js/jquery.min.js',
            'jquery/js/jquery-migrate.js',
            'jquery/js/jquery.blockUI.js',
            'bootstrap/js/bootstrap.min.js',
            'angularjs/js/angular.min.js',
            'angularjs/js/angular-cookies.js',
            'scaffold/js/base.js',
        ]
        css = {
            'all': ('bootstrap/css/bootstrap.css',
                    'scaffold/css/base.css')
        }

class UIModuleDefiningClass(UIDefiningClass, Singleton):
    "Metaclass for UIModule classes"
    def __new__(cls, name, bases, attrs):
        return super(UIModuleDefiningClass, cls).__new__(cls, name, bases, attrs)

# Apply the decorator again to re-bind __unicode__ to the new __str__ method
@python_2_unicode_compatible
class UIModule(BaseUI):
    "Base class for all UI modules which can be rendered as html block in a page"
    __metaclass__ = UIModuleDefiningClass
    __abstract__ = True

    def __str__(self):
        return mark_safe(self.render())

class TemplateUIModule(UIModule):
    "Base class for all UI modules which can be rendered as html block in a page"
    __metaclass__ = UIModuleDefiningClass
    __abstract__ = True
    template = None
    context = {}

    @overrides(UIModule)
    def render(self, **context):
        context.update(self.context)
        return self.template.render(Context(context))


class Site(object):
    __metaclass__ = Singleton

    def __init__(self, title=None):
        self.title = title
        self._registry = {}
        self._portals = []
        self._urls = []
        self.default_portal = None

    def warn(self, msg):
        print >> sys.stderr, "[WARNING] <%s> %s" % (self.__class__.__name__, msg)

    def register(self, portal_class):
        if not issubclass(portal_class, Portal):
            self.warn("the class to register is not a subclass of Portal")
            return None
        if portal_class.uid in self._registry:
            self.warn("ignore the portal class whose uid is '%s' and already registered"
                      % portal_class.uid)
            return None
        portal = portal_class()
        portal.site = self
        self._urls.append(portal.urlpattern)
        for page in portal:
            self._urls.append(page.urlpattern)
        self._registry[portal.uid] = portal
        self._portals.append(portal)
        if self.default_portal is None:
            self.default_portal = portal
            self._urls.append(url(r'^%s$' % settings.URL_PREFIX,
                                  self.default_portal.as_view(),
                                  name=self.default_portal.path()))
        return portal

    @property
    def urls(self):
        return patterns('', *self._urls)

    def __iter__(self):
        return iter(self._portals)

    def autodiscover(self):
        import copy
        from django.utils.importlib import import_module
        from django.utils.module_loading import module_has_submodule

        for app in settings.INSTALLED_APPS:
            mod = import_module(app)
            # Attempt to import the app's UI
            try:
                before_import_registry = copy.copy(self._registry)
                import_module('%s.ui' % app)
            except Exception, e:
                # Reset the model registry to the state before the last import as
                # this import will have to reoccur on the next request and this
                # could raise NotRegistered and AlreadyRegistered exceptions
                # (see #8245).
                self._registry = before_import_registry

                # Decide whether to bubble up this error. If the app just
                # doesn't have an admin module, we can ignore the error
                # attempting to import it, otherwise we want it to bubble up.
                if module_has_submodule(mod, 'ui'):
                    raise UIException("failed to import %s's UI: %s" % (app, str(e)))

site = Site(settings.SITE_NAME)
