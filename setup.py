from setuptools import setup, find_packages

setup(
    name="scaffold",
    version="3.1",
    description="A framework of admin site",
    author='Andrew Wang',
    author_email='andrew.dong.wang@gmail.com',
    license="LGPL",
    packages=find_packages(),
    include_package_data=True,
    scripts=[],
)
